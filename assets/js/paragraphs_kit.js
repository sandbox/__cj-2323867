(function ($) {
  Drupal.behaviors.paragraphs_kit = {
    attach: function (context, settings) {

      jQuery('.paragraphs-item-slide-show .field-name-field-paragraphs-kit-images .field-items').cycle({
        'slides': "> div"
      });

      if (typeof Drupal.settings.paragraphs_kit == 'undefined') {
        return;
      }
      if (typeof Drupal.settings.paragraphs_kit.map == 'undefined') {
        return;
      }

      if (Drupal.settings.paragraphs_kit.map.provider == 'google') {
        _paragraphs_kit_add_google_map(Drupal.settings.paragraphs_kit.map);
      }
      else if (Drupal.settings.paragraphs_kit.map.provider == 'bing') {
        _paragraphs_kit_add_bing_map(Drupal.settings.paragraphs_kit.map);
      }

    }
  };

  /**
   * Add Google map.
   */
  function _paragraphs_kit_add_google_map(settings) {
    google.maps.event.addDomListener(window, 'load', function (){

      var latLng = new google.maps.LatLng(settings.lat, settings.lng);

      var mapOptions = {
        center: latLng,
        zoom: 8
      };

      var map = new google.maps.Map(document.getElementById("map"), mapOptions);

      var marker = new google.maps.Marker({position: latLng});
      marker.setMap(map);

    });
  }

  /**
   * Add Bing map.
   */
  function _paragraphs_kit_add_bing_map(settings) {

    var latLng = new Microsoft.Maps.Location(settings.lat, settings.lng);

    var map = new Microsoft.Maps.Map(document.getElementById("map"), {
      credentials: settings.api_key,
      //credentials:"Aji8nCcY2Fiaj4o-ttdt_FriaEFX_BsbPGmDMK_MYuxj6wF7Q95c7nfe4PHe5CMz",
      center: latLng,
      zoom: 8
    });

    var pin = new Microsoft.Maps.Pushpin(latLng);
    map.entities.push(pin);
  }

})(jQuery);
