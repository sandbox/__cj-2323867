<?php
/**
 * @file
 * paragraphs_kit_advanced_page.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function paragraphs_kit_advanced_page_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_advanced_body'
  $field_bases['field_advanced_body'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_advanced_body',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'paragraphs',
    'settings' => array(),
    'translatable' => 0,
    'type' => 'paragraphs',
  );

  return $field_bases;
}
