<?php
/**
 * @file
 * paragraphs_kit_advanced_page.features.inc
 */

/**
 * Implements hook_node_info().
 */
function paragraphs_kit_advanced_page_node_info() {
  $items = array(
    'advanced_page' => array(
      'name' => t('Advanced Page'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
