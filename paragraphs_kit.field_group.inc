<?php
/**
 * @file
 * paragraphs_kit.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function paragraphs_kit_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_wrapper|paragraphs_item|jumbotron|default';
  $field_group->group_name = 'group_wrapper';
  $field_group->entity_type = 'paragraphs_item';
  $field_group->bundle = 'jumbotron';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Wrapper',
    'weight' => '0',
    'children' => array(
      0 => 'field_jumbotron_title',
      1 => 'field_jumbotron_text',
      2 => 'field_jumbotron_link',
    ),
    'format_type' => 'html-element',
    'format_settings' => array(
      'label' => 'Wrapper',
      'instance_settings' => array(
        'classes' => 'jumbotron',
        'element' => 'div',
        'show_label' => '0',
        'label_element' => 'div',
        'attributes' => '',
      ),
    ),
  );
  $export['group_wrapper|paragraphs_item|jumbotron|default'] = $field_group;

  return $export;
}
