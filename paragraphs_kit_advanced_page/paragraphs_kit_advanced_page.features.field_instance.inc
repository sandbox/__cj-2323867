<?php
/**
 * @file
 * paragraphs_kit_advanced_page.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function paragraphs_kit_advanced_page_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-advanced_page-field_advanced_body'
  $field_instances['node-advanced_page-field_advanced_body'] = array(
    'bundle' => 'advanced_page',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'paragraphs',
        'settings' => array(
          'view_mode' => 'full',
        ),
        'type' => 'paragraphs_view',
        'weight' => 1,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_advanced_body',
    'label' => 'Advanced Body',
    'required' => 0,
    'settings' => array(
      'allowed_bundles' => array(
        'faqs' => 'faqs',
        'jumbotron' => 'jumbotron',
        'map' => 'map',
        'section_header' => 'section_header',
        'slide_show' => 'slide_show',
        'text' => 'text',
      ),
      'default_edit_mode' => 'open',
      'title' => 'Paragraph',
      'title_multiple' => 'Paragraphs',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'paragraphs',
      'settings' => array(),
      'type' => 'paragraphs_embed',
      'weight' => 41,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Advanced Body');

  return $field_instances;
}
