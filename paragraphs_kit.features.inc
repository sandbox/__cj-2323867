<?php
/**
 * @file
 * paragraphs_kit.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function paragraphs_kit_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_paragraphs_info().
 */
function paragraphs_kit_paragraphs_info() {
  $items = array(
    'faqs' => array(
      'name' => 'FAQs',
      'bundle' => 'faqs',
      'locked' => '1',
    ),
    'jumbotron' => array(
      'name' => 'Jumbotron',
      'bundle' => 'jumbotron',
      'locked' => '1',
    ),
    'map' => array(
      'name' => 'Map',
      'bundle' => 'map',
      'locked' => '1',
    ),
    'slide_show' => array(
      'name' => 'Slide show',
      'bundle' => 'slide_show',
      'locked' => '1',
    ),
    'text' => array(
      'name' => 'Text',
      'bundle' => 'text',
      'locked' => '1',
    ),
  );
  return $items;
}
